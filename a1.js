db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "fruitsOnsale" }]);

db.fruits.aggregate([
  { $match: { stock: { $gte: 20 } } },
  { $count: "enough_Stock" },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", average_price: { $avg: "$price" } } },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", highest_price: { $max: "$price" } } },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", lowest_price: { $min: "$price" } } },
]);
